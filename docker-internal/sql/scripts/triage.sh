#!/bin/bash

set -e

start_date=$1
end_date=$2

mysql -u jenkins -p$(cat /run/secrets/mysql-pw-jenkins) -e "SHOW DATABASES;" -s --skip-column-names | grep -v '^information_schema$\|^performance_schema$\|^mysql$\|^sys$' | while read db; do mysql -u jenkins -p$(cat /run/secrets/mysql-pw-jenkins) -D "$db" -e "SELECT '$db' as database_name, COUNT(*) FROM (SELECT sha, start_time, description FROM ${db}.revision JOIN ${db}.build ON revision.build_id = build.build_id WHERE project = 'mesa' AND start_time > '$start_date' AND start_time < '$end_date' GROUP BY sha) AS subquery;" -s --skip-column-names | sed -e 's/\t/,/g'; done
